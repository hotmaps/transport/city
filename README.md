[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687644.svg)](https://doi.org/10.5281/zenodo.4687644)

# Vehicle stock at city level

The repository contains the number of vehicles registered. 
The values are estimated until 2030 appling the buisness as usual scenario, starting from the registered values of the years: `1989_1993`, `1994_1998`, `1999_2002`, `2003_2006`.


# References
[Urban Audit, eurostat](https://www.eea.europa.eu/data-and-maps/data/external/urban-audit-database)

